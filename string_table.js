var StringTable = {};

StringTable.randomSport = () => {
  var strTypes = [
    "a football game",
    "a soccer game",
    "a BCS tournament",
    "a basketball game",
    "a boxing match",
    "a wrestling match",
    "a speedkart race",
    "the literal weather channel (seriously?)",
    "a game of tennis",
    "a jeep sumo tournament",
    "a game of falling tiles",
    "a push broom tournament",
    "a badminton game",
    "a drama thread",
    "a building competition",
    "a card game",
    "casino royale",
    "a horse race",
    "a dancing dog"
  ];

  var selection = Math.round(Math.random() * (strTypes.length-1));
  return strTypes[selection];
};

// StringTable.randomPrisonGame

StringTable.jobStr = (jobTrack, profit) => {
  var strTypes;
  switch(jobTrack) {
    case "labor":
      strTypes = [
        `You scrap some metal`,
        `You go dumpster diving`,
        `You pick up some cash off the street`,
        `You pick up cash while walking through a firefight in the streets`,
        `You go mining`
      ]
    break;

    case "police":
      strTypes = [
        `You patrol the city`,
        `You spend your day filling out paperwork`
      ]
    break;

    case "medical":
      strTypes = [
        `You complete your shift`
      ]
    break;

    case "business":
      strTypes = [
        `You tend to your investments`,
        `You refinance your business`,
        `Business-wise, this all looks like appropriate business! You turn in for the day`
      ]
    break;

    case "prison":
      strTypes = [
        `Your toilet recipes are popular with the inmates! You sell food`,
        `You sell your meal for the day`,
        `You hustle an inmate in a game of cards`
      ]
    break;
  }

  var selection = Math.round(Math.random() * (strTypes.length-1));
  var strPrefix = strTypes[selection];
  var finalStr = `${strPrefix} and earn :dollar: ${profit}.`;
  return finalStr;
}

module.exports = StringTable;