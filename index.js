const Discord = require("discord.js");
const client = new Discord.Client();
const StringTable = require("./string_table.js");

var mysql = require("mysql2");
const config = require("./config.json");
const color = 3962879;
const tableColumnsProfile = "money BIGINT DEFAULT 0, jobtrack VARCHAR(255), last_daily DATETIME, "
+ "has_robbed_today BOOL DEFAULT false, has_purchased_lot_today BOOL DEFAULT false, "
+ "autobet_enabled BOOL DEFAULT false, robbing_unlocked BOOL DEFAULT false, "
+ "realestate_unlocked BOOL DEFAULT FALSE, last_bet DATETIME";
var commands = {};

const jobPay = {
  "labor": 10,
  "police": 20,
  "medical": 40,
  "business": 40,
  "prison": 10,
}

var db = mysql.createConnection({
  host     : config.mysql_host,
  user     : config.mysql_user,
  password : config.mysql_password,
  database : "minicityrpg_userdata"
});

function queryAsync(query) {
  return new Promise((resolve, reject) => {
    db.query(query, (err, results, fields) => {
      if (err) reject(err);
      resolve( { results, fields } );
    });
  });
}

commands.help = (msg) => {
  msg.channel.send( { embed: { 
    color, 
    description: "Welcome to Mini CityRPG!",
    fields: [ 
      { name: "Commands", 
        value: `city!help\ncity!balance\ncity!daily\ncity!bet (Cost: :dollar: 10)`,
        inline: true
      },
    ]
  }} );
};

commands.daily = async (msg) => {
  var id = msg.author.id;
  var data = await getData(id);

  // Set track if not already defined
  if(data.jobtrack === null) {
    setData(id, "jobtrack", "labor");
    data.jobtrack = "labor";
  }

  var hasClaimedDaily = await checkDate(id, "last_daily");
  //var hasClaimedDaily = false;
  
  if(hasClaimedDaily) {
    msg.channel.send({embed: { color, description: "You have already earned your pay for today!"}})
    return;
  }
  
  refreshDate(id, "last_daily");

  var moneyPayout = getRandomPay(data.jobtrack);

  var date = new Date();

  var title = `Today, on ${date.toLocaleString("default", { month: "long"})} ${date.getDate()}, year ${date.getFullYear()}...`;
  var jobStr = StringTable.jobStr("labor", moneyPayout);

  addData(id, "money", moneyPayout);

  var newBal = data.money + moneyPayout;
  var dataStr = `${jobStr}\nYour new account balance is :dollar: ${newBal}`;

  msg.channel.send({embed: { color, title, description: dataStr}})
};

commands.bet = async (msg) => {
  var id = msg.author.id;
  var data = await getData(id);
  const betCost = 10;
  subtractData(id, "money", betCost);

  var hasClaimedBet = await checkDate(id, "last_bet");

  if(hasClaimedBet) {
    msg.channel.send({embed: { color, description: "You have already claimed your bet for today!"}});
    return;
  }

  refreshDate(id, "last_bet");

  var betPayout = getRandomBet();
  var betWin = Math.round(Math.random());
  var newBal, resultStr;

  var dataStr = `You bet on ${StringTable.randomSport()}...`;
  var sentMsg = await msg.channel.send({embed: {color, description: dataStr}});

  setTimeout(() => {
    if(betWin) {
      addData(id, "money", betPayout);
      newBal = data.money + betPayout - 10;
  
      resultStr = `**You win**, earning :dollar: ${betPayout}\nYour new account balance is :dollar: ${newBal}`;
    }
    else {
      resultStr = `**You lose!**`;
    }

    sentMsg.edit({embed: {color, description: `${dataStr}\n\n${resultStr}`}});
  }, 1500);
};

commands.balance = async (msg) => {
  var data = await getData(msg.author.id);

  msg.channel.send({embed: { color, description: `Your account balance is :dollar: ${data.money}`}})
};

client.on("ready", () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on("message", msg => {
  var msgCheck = msg.content.toLowerCase();
  if(msgCheck.startsWith("city!help")) {
    commands.help(msg);
  }

  if(msgCheck.startsWith("city!daily")) {
    commands.daily(msg);
  }

  if(msgCheck.startsWith("city!balance")) {
    commands.balance(msg);
  }

  if(msgCheck.startsWith("city!bet")) {
    commands.bet(msg);
  }
});

async function init() {
  db.connect();
 
  client.login(config.discord_token);  

  if(config.first_time_init) {
    initDatabase();
  }
}

async function initTable(table, args) {
  var sql = `CREATE TABLE ${table} (${args})`;

  try {
    await queryAsync(sql);
    console.log(`Created table: ${table}`);
  }
  catch(err) {
    if(err.message.startsWith(`Table '${table}' already exists`)) {
      console.log(`Table exists: ${table}`);
    }
    else {
      throw err;
    }
  }
}

async function initDatabase() {
  db.query("CREATE DATABASE minicityrpg_userdata", function () {
      // No strict error checking on this -- we'll certainly catch it later if there's no database
    console.log("Called create database");
  })

  initTable("profiles", `userid VARCHAR(255) PRIMARY KEY, ${tableColumnsProfile}`);
  initTable("stats", `userid VARCHAR(255) PRIMARY KEY, total_money_earned BIGINT DEFAULT 0, total_money_spent BIGINT DEFAULT 0, jail_sentences_served BIGINT DEFAULT 0`);
}

function initUser(id) {
  queryAsync(`INSERT INTO profiles (userid) VALUES (${id})`);
}

async function getData(id, secondQuery) {
  var res = await queryAsync(`SELECT * FROM profiles WHERE userid = ${id}`);

  var nodata = res.results.length === 0;
  if(nodata && !secondQuery) {
    initUser(id);
    return getData(id, true); // Repeat the query once
  }

  return res.results[0];
}

function refreshDate(id, key) {
  queryAsync(`UPDATE profiles SET ${key} = NOW() WHERE userid = ${id}`);
}

async function checkDate(id, key) {
  var res = await queryAsync(`SELECT ${key} FROM profiles WHERE userid = ${id} AND DATE_ADD(${key}, INTERVAL 16 HOUR) > NOW()`);

  // If we have a result, the date comparison passed and the user has already claimed their daily
  return res.results.length !== 0;
}

function setData(id, key, val) {
  var setter;

  if(typeof val === "string") {
    setter = `'${val}'`;
  }
  else {
    setter = val;
  }

  queryAsync(`UPDATE profiles SET ${key} = ${setter} WHERE userid = ${id}`);
}

function addData(id, key, val) {
  queryAsync(`UPDATE profiles SET ${key} = ${key} + ${val} WHERE userid = ${id}`);
}

function subtractData(id, key, val) {
  queryAsync(`UPDATE profiles SET ${key} = ${key} - ${val} WHERE userid = ${id}`);
}

function getRandomPay(jobtrack) {
  var payFloor = jobPay[jobtrack]*0.90;
  var payCeil = jobPay[jobtrack]*1.20;

  var finalPay = Math.round(Math.random() * (payCeil - payFloor) + payFloor);
  return finalPay;
}

function getRandomBet() {
  var betMid = 20;
  var payFloor = betMid;
  var payCeil = betMid*1.4;

  var finalPay = Math.round(Math.random() * (payCeil - payFloor) + payFloor);
  return finalPay
}

init();